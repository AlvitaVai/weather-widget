
<?php
$apiKey ="Your API key";
$cityId = "2643743";
$googleApiUrl = "http://api.openweathermap.org/data/2.5/forecast?id=" . $cityId . "&lang=en&units=metric&appid=" . $apiKey;

$ch = curl_init();

curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_URL, $googleApiUrl);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_VERBOSE, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$response = curl_exec($ch);

curl_close($ch);
$data = json_decode($response);
$currentTime = time();
date_default_timezone_set('GB');

?>

<!DOCTYPE html>

<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
		<title>Weather Widget</title>
        <link rel="stylesheet" href="assets/css/style.css">      
	</head>
	<body>
	<!-- /DISPLAY CURRENT WEATHER/ -->

		<section class="weather-report">
		<div class="container flex">
			<div class="main-content col">
				<h2><?php echo $data->city->name; ?> Weather</h2>
				<div class="time">
					<div><?php echo "<h3>".date("l H:i", $currentTime) ."</h3>"; ?></div>
					<div><?php echo "<p>" . date("jS F, Y",$currentTime) . "</p>"; ?></div>	
					<span>Humidity: <?php echo $data->list[0]->main->humidity ; ?> %</span>
					<div>Wind: <?php echo $data->list[0]->wind->speed ; ?> km/h</div>
				</div>		
			<div class="weather-forecast">
				<img src="http://openweathermap.org/img/w/<?php echo $data->list[0]->weather[0]->icon; ?>.png";
				class="weather-icon" />
				<div class='weather-info'>
				<?php echo $data->list[0]->main->temp ?>°C <br></div>
				<span> <?php echo ucwords($data->list[0]->weather[0]->description); ?> </span> 		
			</div>
			</div>

	<!-- /DISPLAY THE FUTURE WEATHER/ -->

		<div class = "week-forecast">
			<div class="weather-info flex">
				<?php 
				$start = new DateTime('tomorrow 12:00:00');
				$end = new DateTime('+5 days 12:00:00');
				$interval = new DateInterval('P1D');
				$period = new DatePeriod($start, $interval, $end);
					foreach ($period as $dt) {
						echo "<span class='col'>". "<h3>" . $dt->format('l') . "</h3>"."<br>" ; 
						$filtered_array = array_filter($data->list, function ($key) use ($dt) {
							return new DateTime($key->dt_txt) == $dt;						
						});
						$day_data = array_shift($filtered_array);
						if($day_data !== null) {
							echo '<img src = "http://openweathermap.org/img/w/' . $day_data->weather[0]->icon . '.png">' . "<br>" ;									
							echo "<span class='max-tem'>" . $day_data->main->temp_max . "°C" . "<br>" . "</span>";	
																						
						}						
						$dt->modify('+12 hours'); 
						$filtered_array = array_filter($data->list, function ($key) use ($dt) {
							return new DateTime($key->dt_txt) == $dt;								
						});
						$day_data = array_shift($filtered_array);
						if($day_data !== null) {
							echo "<span class='min-tem'>" . $day_data->main->temp_min . "°C" . "<br>" . "</span>" . "</span>" ; 
										
						}
					}
				?>
			</div>
		</div>
		</div>
		</section>
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		<script src="assets/scripts/custom.js"></script>
	</body>
</html>
